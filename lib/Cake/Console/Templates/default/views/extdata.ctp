<?php echo '<?php '; ?>
echo $this->request->query['_callback']; <?php echo '?>' ?>({
	"_success":true,
	"_total" :"<?php echo '<?php' ?> echo $this->Paginator->counter(array('format' => __('{:count}'))); <?php echo '?>'?>", 
	"_message":"Loaded data",	
	"_data":[
    <?php echo "<?php foreach (\${$pluralVar} as \${$singularVar}): ?>\n"; ?>
	<?php $string = '{';?>
	<?php foreach ($fields as $field){
		$uu = ucfirst($singularVar);
		$lu = strtolower($singularVar);
		
		$field_name = "\$".$singularVar."['".$uu."']['".$field."']";
		$string .= '"'.$field.'":';
		if($this->templateVars['schema'][$field]['type']=="integer"){
				
				$string .= "<?php if(".$field_name.") {echo ".$field_name.".','; } else {  echo 'null,';}?>";
				}
			else					
				
					$string .= "<?php \$x= json_encode(".$field_name."); if(\$x!='') echo \$x; else echo '".'""'."';?>,";
	} ?>
	<?php $string = rtrim($string,',');
	$string .= '},'; echo $string; ?>
	
		
<?php echo '<?php' ?> endforeach; <?php echo '?>'?>
	]
});
